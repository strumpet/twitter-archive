# renovate: datasource=docker depName=nginx
ARG NGINX_VERSION=1.21.6
FROM nginx:$NGINX_VERSION-alpine

COPY archive /usr/share/nginx/html/
